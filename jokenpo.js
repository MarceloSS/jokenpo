let eRock = document.getElementById("eRock")
let ePaper = document.getElementById("ePaper")
let eScisors = document.getElementById("eScisors")
let Rock = document.getElementById("Rock")
let Paper = document.getElementById("Paper")
let Scisors = document.getElementById("Scisors")
let reset = document.getElementById("reset")
let result = document.getElementById("result")
//pontuação
let wScore = document.getElementById("win")
let lScore = document.getElementById("loss")
//turnos
let turns = document.getElementById("turns")
let turn = 0
//todos os botoes
let plChoices = [Rock, Paper, Scisors]
let pcChoices = [eRock, ePaper, eScisors]
//condições
let wins = [["Rock", "Scisors"],["Paper", "Rock"],["Scisors", "Paper"]]
//pontuações
let plScore = 0
let pcScore = 0
let win = 0
let loss = 0
//escondendo escolhas do computador inicialmente
for (options in pcChoices){
    pcChoices[options].style.opacity = 0
}
//randomizando
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
//botão de reset
reset.onclick = function(){
    turns.innerHTML = "0/3"
    turn = 0
    plScore = 0
    pcScore = 0
    result.innerHTML = ""
    for (c in plChoices){
        pcChoices[c].style.opacity = 0
        plChoices[c].disabled = false
    }
}
//escondendo opções menos escolha
function pcTurn(choice){
    for(c in pcChoices){
        if(pcChoices[c] != choice){
            pcChoices[c].style.opacity = 0
        }else{
            pcChoices[c].style.opacity = 100
        }
    }
}
//escolha do computador
function pcChoice(){
    let choice = getRandomInt(0, 9)
    console.log(choice)
    if (choice == 0 || choice == 3 || choice == 6){
        pcTurn(eRock)
        return "Rock"
    }if (choice == 1 || choice == 4 || choice == 7){
        pcTurn(ePaper)
        return "Paper"
    }else{
        pcTurn(eScisors)
        return "Scisors"
    }
}

//evento de escolha
const choose = function(event){
    if(turn < 3){
        let choice = event.currentTarget.id
        let pc = pcChoice()
        //resultado + impressão na tela
        result.appendChild(document.createTextNode(checkWin(choice, pc)))
        console.log(turn);
    }
}
//imprimindo placar
function placar(){
    for(c in plChoices){
        plChoices[c].disabled = true
    }
    if (plScore > pcScore){
        win++
        wScore.innerHTML = " "
        wScore.appendChild(document.createTextNode(win))
        return "YOU WIN, CONGRATS"
    }else{
        loss++
        lScore.innerHTML = " "
        lScore.appendChild(document.createTextNode(loss))
        return "YOU LOST, TRY AGAIN"
    }
}
//checando resultado
function checkWin(player, pc){
    result.innerHTML = ""
    for(cond in wins){
        if(wins[cond][0] == player && wins[cond][1] == pc){
            turn ++
            turns.innerHTML = ""
            turns.appendChild(document.createTextNode(`${turn}/3`))
            plScore++
            //finalizando jogo
            if(turn < 3){
                return "WINS"
            }else{
                return placar()
            }
        }else if(wins[cond][0] == pc && wins[cond][1] == player){
            turn ++
            turns.innerHTML = ""
            turns.appendChild(document.createTextNode(`${turn}/3`))
            pcScore++
            //finalizando jogo
            if(turn < 3){
                return "LOSES"
            }else{
                return placar()
            }
        }
    }
    return "TIES"
}
//adicionando funcionalidade aos botoes
for (c in plChoices) {
    plChoices[c].addEventListener('click', choose);
}



